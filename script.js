//Теорія.
//1. Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.
// Цикли дозволяють виконувати велику кількість операцій в скороченій формі.
// Приклади - нижче в виконаних задачах.

//2. Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.
// While. Перевірка виконання умов вводу чисел: якщо умова не виконується,
// повторно вивести запит числа.
// For. Виконання операцій для кожного з чисел в діапазоні.

//3. Що таке явне та неявне приведення (перетворення) типів даних у JS?
// Неявне перетворення типів - викорнується JS автоматично при виконанні операцій
// порівняння, математичних операціях.
// Явне перетворення типів - виконуються в програмі "руками розробника". Наприклад,
// можна використати фунції Number(), String().


// 1. Оброблюються цілі числа, як додатні так і від'ємні. Число 0 пропускається.
// В умовах задачі сказано:
// Вивести в консолі всі числа, кратні 5, від 0 до введеного користувачем
// числа. Якщо таких чисел немає - вивести в консоль фразу "Sorry, no numbers".
// Якщо враховувати 0, то фраза "Sorry, no numbers" ніколи не буде виведена.
// Тому що 0 ділиться на 5 без залишку (0/5 = 0).
// Враховуючи це, користувач може ввести числа від 0 (при цьому сам 0 не включений
// до розгляду).
// P.S. Стилі мінімальні, без reset.css тощо..

let btn1 = document.querySelector(".first_task");
let btn2 = document.querySelector(".second_task");


function first_task() {    

    let number = prompt("Enter your number");
    let match = -200;

    while (+number - Math.trunc(+number) !== 0 || number === "") {
        number = prompt("Enter your number");
    }

    if (number !== null) {
        if (+number >= 0) {
            for (let i = 0; i <= +number; i++) {
                if (i%5 === 0 && i !== 0) {
                    console.log(i);
                    match = 1;
                }
            }
            if (match === -200) {
                console.log("Sorry, no numbers");
            }
        }

        if (+number <= 0) {
            for (let i = 0; i >= +number; i--) {
                if (i%5 === 0 && i !== 0) {
                    console.log(i);
                    match = 1;
                }
            }
            if (match === -200) {
                console.log("Sorry, no numbers");
            }
        }
    }
}

// 2.

function second_task() {

    let m;
    let n;
    let check = 0;
    let number_present = 0;

    m = prompt("Enter start number");
    while (m === "") {
        m = prompt("Enter start number");
    }

    if (m !== null) {

        n = prompt("Enter end number");
    
        while (n === "") {
            n = prompt("Enter end number");
        }
    
    }

    if (n !== null && n !== "") {
        while ((+m >= +n) && n !== null) {
            if (n !== null) {
                m = prompt("Enter start number");
            }
            if (m !== null) {
                n = prompt("Enter end number");
            }
        }
    
        // simple numbers
        for (let i = +m; i <= +n; i++) {

            check = 0;
            if (i >= 2) {
                // делим i на все натур.числа, начиная с 2 до n
                for (let j = 2; j <= +n; j++) {

                    if (i % j === 0) {
                        check++;
                    }
                }
                if (check === 1) {
                    console.log(i);
                    number_present = 1;
                }
            }
        }

        if (number_present === 0) {
            console.log("Sorry, no simple numbers..");
        }
    }
}

btn1.onclick = first_task;
btn2.onclick = second_task;




